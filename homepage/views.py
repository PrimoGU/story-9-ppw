from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .models import Status
from .forms import Status_Form

# Create your views here.
response = {}

def index_view(request):
	status = Status.objects.all().order_by('-created_date')
	response['status'] = status
	response['status_form'] = Status_Form
	return render(request, 'index.html', response)

def add_status_view(request):
	form = Status_Form(request.POST or None)

	if( (request.method == 'POST') and form.is_valid() ):
		response['title'] = request.POST['title']
		response['description'] = request.POST['description']
		status = Status(title=response['title'], description=response['description'])
		status.save()
		return HttpResponseRedirect('/')
	else:
		return HttpResponseRedirect('/')

def del_status_view(request, id):
	Status.objects.get(pk=id).delete()
	return HttpResponseRedirect('/')
