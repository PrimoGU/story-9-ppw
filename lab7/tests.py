from django.test import TestCase, LiveServerTestCase
from django.urls import resolve
from django.utils import timezone
from django.http import HttpRequest

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions

from urllib.parse import urljoin
from . import views

# Create your tests here.

class Lab7UnitTest(TestCase):

        #test url '/lab7' exist or not
    def test_url_exist(self):
        response = self.client.get('/lab7/')
        self.assertEqual(response.status_code, 200)

        #test url using status view function
    def test_url_calling_right_views_func(self):
        found = resolve('/lab7/')
        self.assertEqual(found.func, views.lab7_view)

        #test '/lab7/' using templates lab7.html
    def test_url_using_right_template(self):
        response = self.client.get('/lab7/')
        self.assertTemplateUsed(response, 'lab7.html')

        #test response html contain "Status"
    def test_template_contains_Activity_Experience_Achievements(self):
        request = HttpRequest()
        response = views.lab7_view(request)
        self.assertContains(response, 'Activity')
        self.assertContains(response, 'Experience')
        self.assertContains(response, 'Achievements')

    def test_using_right_staticfiles(self):
        response = self.client.get('/lab7/')
        self.assertContains(response, 'static/css/lab7_style')
        self.assertContains(response, 'static/js/lab7_script')


class Lab7FunctionalTest(LiveServerTestCase):

        #setup selenium browser
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver.exe', chrome_options=chrome_options)
        super(Lab7FunctionalTest, self).setUp()

        #set tearing down
    def tearDown(self):
        self.browser.quit()
        super(Lab7FunctionalTest, self).tearDown()

        #test opening the web
    def test_open(self):
        url = urljoin(self.live_server_url, '/lab7/')
        self.browser.get(url)
        self.assertEqual(self.browser.title, "PPW Story 7 | Primo Giancarlo Uneputty")
        self.assertIn("Activity", self.browser.page_source)
        self.assertIn("Experience", self.browser.page_source)
        self.assertIn("Achievements", self.browser.page_source)

    def test_dark_mode(self):
        url = urljoin(self.live_server_url, '/lab7/')
        self.browser.get(url)
        toggle = self.browser.find_element_by_id("toggle")
        body = self.browser.find_element_by_tag_name('body')
        
        self.assertEqual(body.value_of_css_property("backgroundColor"), 'rgba(255, 255, 255, 1)')
        toggle.click()
        self.assertEqual(body.value_of_css_property("backgroundColor"), 'rgba(49, 49, 49, 1)')
