from django.shortcuts import render

# Create your views here.

def lab7_view(request):
    return render(request, 'lab7.html')
