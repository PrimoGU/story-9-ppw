from django.test import TestCase, LiveServerTestCase
from django.urls import resolve
from django.utils import timezone
from django.http import HttpRequest

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium import webdriver

from urllib.parse import urljoin
from . import views

# Create your tests here.

class Lab8UnitTest(TestCase):

	#Test url '/lab8/' exist or not
	def test_url_is_exist(self):
		response = self.client.get('/lab8/')
		self.assertEqual(response.status_code, 200)

	#Test url using status view function
	def test_url_calling_right_views_function(self):
		found = resolve('/lab8/')
		self.assertEqual(found.func, views.lab8_view)

	#Test '/lab8/' using templates 'lab8.html'
	def test_url_using_right_template(self):
		response = self.client.get('/lab8/')
		self.assertTemplateUsed(response, 'lab8.html')

	#Test response html contain "Book" or not
	def test_template_contain_Book(self):
		request = HttpRequest()
		response = views.lab8_view(request)
		self.assertContains(response, 'Book')

	#Test '/lab8/' using static or not
	def test_using_right_staticfiles(self):
		response = self.client.get('/lab8/')
		self.assertContains(response, 'static/css/lab8_style')
		self.assertContains(response, 'static/js/lab8_script')

class Lab8FunctionalTest(LiveServerTestCase):

	#Setup selenium browser using chromedriver
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.browser = webdriver.Chrome('./chromedriver.exe', chrome_options=chrome_options)
		super(Lab8FunctionalTest, self).setUp()

	#Set tearing down selenium browser
	def tearDown(self):
		self.browser.quit()
		super(Lab8FunctionalTest, self).tearDown()

	#test opening the web
	def test_open(self):
		url = urljoin(self.live_server_url, '/lab8/')
		self.browser.get(url)
		self.assertEqual(self.browser.title, "PPW Story 8 | Primo Giancarlo Uneputty")
		self.assertIn("Book", self.browser.page_source)
