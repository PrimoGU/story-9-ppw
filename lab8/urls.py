from django.urls import path, include
from . import views

app_name = 'lab8'

urlpatterns = [
    path('', views.lab8_view, name='view_lab8'),
]
