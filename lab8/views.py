from django.shortcuts import render

# Create your views here.

def lab8_view(request):
    return render(request, 'lab8.html')
