from django.test import TestCase
from django.http import HttpRequest
from django.urls import resolve
from django.utils import timezone
from . import views

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium import webdriver

#Create your tests here
class Lab9UnitTest(TestCase):

	#Test url '/lab9/' exist or not
	def test_lab9_url_exist(self):
		response = self.client.get('/lab9/')
		self.assertEqual(response.status_code, 302)
		response = self.client.get('/lab9/signup/')
		self.assertEqual(response.status_code, 200)
		response = self.client.get('/lab9/login/')
		self.assertEqual(response.status_code, 200)
		response = self.client.get('/lab9/logout/')
		self.assertEqual(response.status_code, 302)

	#Test Lab 9 url using right views functions
	def test_lab9_calling_right_views_function(self):
		found = resolve('/lab9/')
		self.assertEqual(found.func, views.lab9_view)
		found = resolve('/lab9/signup/')
		self.assertEqual(found.func, views.signup_view)
		found = resolve('/lab9/login/')
		self.assertEqual(found.func, views.login_view)
		found = resolve('/lab9/logout/')
		self.assertEqual(found.func, views.logout_view)

	#Test '/lab9/' using right templates
	def test_lab9_using_right_template(self):
		response = self.client.get('/lab9/signup/')
		self.assertTemplateUsed(response, 'signup_lab9.html')
		response = self.client.get('/lab9/login/')
		self.assertTemplateUsed(response, 'login_lab9.html')

	#Test '/lab9/' using right CSS
	def test_lab9_using_right_staticfiles(self):
		response = self.client.get('/lab9/signup/')
		self.assertContains(response, 'static/css/lab9_style')
		response = self.client.get('/lab9/login/')
		self.assertContains(response, 'static/css/lab9_style')

class Lab9FunctionalTest(TestCase):

	#Setup selenium browser
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(Lab9FunctionalTest, self).setUp()

	#Set tearing down
	def tearDown(self):
		self.browser.quit()
		super(Lab9FunctionalTest, self).tearDown()

	#Test opening the web
	def test_open(self):
		self.browser.get('http://localhost:8000/lab9/login/')
		self.assertEqual(self.browser.title, "Login Page")
		self.assertIn("LOG IN HERE!", self.browser.page_source)
		self.browser.get('http://localhost:8000/lab9/signup/')
		self.assertEqual(self.browser.title, "Signup Page")
		self.assertIn("SIGN UP HERE!", self.browser.page_source)

	#Test making new user
	def test_signup(self):
		self.browser.get('http://localhost:8000/lab9/signup/')
		username = self.browser.find_element_by_id("id_username")
		username.send_keys("admin")
		email = self.browser.find_element_by_id("id_email")
		email.send_keys("equiv.onyx@gmail.com")
		password1 = self.browser.find_element_by_id("id_password1")
		password1.send_keys("adminpassword123")
		password2 = self.browser.find_element_by_id("id_password2")
		password2.send_keys("adminpassword123")
		password2.submit()
		self.assertIn("A user with that username already exists.", self.browser.page_source)

	#Test logging in to user
	def test_login(self):
		self.browser.get('http://localhost:8000/lab9/login/')
		username = self.browser.find_element_by_id("id_username")
		username.send_keys("admin")
		password = self.browser.find_element_by_id("id_password")
		password.send_keys("adminpassword123")
		password.submit()
		self.assertIn("admin !", self.browser.page_source)
