from django.urls import path, include
from . import views

app_name = 'lab9'

urlpatterns = [
	path('', views.lab9_view, name = "view_lab9"),
	path('signup/', views.signup_view, name = "signup"),
	path('login/', views.login_view, name = "login"),
	path('logout/', views.logout_view, name = "logout"),
]