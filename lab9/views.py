from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth import login, logout
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.decorators import login_required
from .forms import UserRegisterForm

# Create your views here.
def signup_view(request):
	if request.method == 'POST':
		form = UserRegisterForm(request.POST)
		if form.is_valid():
			username = form.cleaned_data.get('username')
			user = form.save()
			login(request, user)
			return redirect('lab9:view_lab9')
	else:
		form = UserRegisterForm()
	return render(request, 'signup_lab9.html', {'form':form})

def login_view(request):
	if request.method == 'POST':
		form = AuthenticationForm(data=request.POST)
		if form.is_valid():
			user = form.get_user()
			login(request, user)
			return redirect('lab9:view_lab9')
	else:
		form = AuthenticationForm()
	return render(request, 'login_lab9.html', {'form':form})

@login_required(login_url="/lab9/login/")
def logout_view(request):
	if request.method == 'POST':
		logout(request)
		return redirect('lab9:login')

@login_required(login_url="/lab9/login/")
def lab9_view(request):
	return render(request, 'hello_lab9.html')
