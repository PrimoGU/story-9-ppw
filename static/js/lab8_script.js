function search(keyword){
    $("#result")[0].innerHTML = "<p class='m-auto mt-3 mb-3 text-align-center'>Loading....<p>"
    $.ajax({
        url: "https://www.googleapis.com/books/v1/volumes?q="+keyword,
        success: function(result){
            maxPage = Math.floor(result.totalItems/itemPerPage);
            currentPage = 0;
            key = keyword
            paginateNav();
            $("#result")[0].innerHTML = ""
            for(i=0; i<result.items.length; i++){
                $("#result")[0].innerHTML += bookFormat(result.items[i]);
            }
        }
    })
}
var itemPerPage=10;
var maxPage;
var currentPage;
var key;

$('#search').keypress(function(event){
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if(keycode == '13'){
        search(event.target.value);
    }
});

$('#next').click(function(){
    if(currentPage<maxPage){
        currentPage+=1;
        $("#result")[0].innerHTML = "<p class='m-auto mt-3 mb-3 text-align-center'>Loading....<p>"
        $.ajax({
            url: "https://www.googleapis.com/books/v1/volumes?q="+key+"&startIndex="+10*currentPage,
            success: function(result){
                paginateNav();
                $("#result")[0].innerHTML = ""
                for(i=0; i<result.items.length; i++){
                    $("#result")[0].innerHTML += bookFormat(result.items[i]);
                }
            }
        })
    }
});

$('#prev').click(function(){
    if(currentPage>0){
        currentPage-=1;
        $("#result")[0].innerHTML = "<p class='m-auto mt-3 mb-3 text-align-center'>Loading....<p>"
        $.ajax({
            url: "https://www.googleapis.com/books/v1/volumes?q="+key+"&startIndex="+10*currentPage,
            success: function(result){
                paginateNav();
                $("#result")[0].innerHTML = ""
                for(i=0; i<result.items.length; i++){
                    $("#result")[0].innerHTML += bookFormat(result.items[i]);
                }
            }
        })
    }
});

function paginateNav(){
    if(currentPage>0){
        $('#prev')[0].className = "page-item"
    } else {
        $('#prev')[0].className = "page-item disabled"
    }
    if(currentPage<maxPage){
        $('#next')[0].className = "page-item"
    } else {
        $('#next')[0].className = "page-item disabled"
    }

    $('#current')[0].innerHTML = currentPage+1;
}

function bookFormat(book){
    var {infoLink, imageLinks, title, authors} = book.volumeInfo;
    return (
       " <a href="+infoLink+" target='blank' class='no m-auto'><div class='item m-4'>"+
        "<img src="+(imageLinks ? imageLinks.thumbnail : null )+" alt='' class='mt-3 mr-3 ml-3'/>"+
        "<div class='book-title text-align-center'><p class='p-1 m-0'>"+title+"</p>"+
        "<p class='p-1 m-0'>by "+((authors) ? authors[0] : "unknown") +"</p></div></div></a>"
    )
}
